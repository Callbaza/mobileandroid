package com.example.alex.myapplication;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.alex.myapplication.sync.CheckConnection;
import com.example.alex.myapplication.ui.MenuFragment;
import com.example.alex.myapplication.ui.NoteListFragment;

import java.util.Timer;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        this.setTitle("Note");
        NoteListFragment noteListFragment = new NoteListFragment();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, noteListFragment)
                    .commitNow();
        }
    }


    public void replaceFragments(Class fragmentClass) {
        NoteListFragment fragment = null;
        try {
            fragment = (NoteListFragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, fragment)
                .commit();
    }
}