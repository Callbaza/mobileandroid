package com.example.alex.myapplication.sync;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.Nullable;
import com.example.alex.myapplication.repo.NoteRepository;
import com.example.alex.myapplication.viewmodel.NotesViewModel;
import com.example.alex.myapplication.vo.Nota;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

public class CheckConnection extends TimerTask {
    private Context context;
    private LifecycleOwner owner;
    private NoteRepository noteRepository;
    private List<Nota> note = new ArrayList<>();
    private NotesViewModel mViewModel;
    public CheckConnection(Context context, LifecycleOwner owner, NotesViewModel mViewModel){
        this.context = context;
        this.owner = owner;
        this.noteRepository = new NoteRepository(context);
        this.mViewModel = mViewModel;
    }

    @Override
    public void run() {
        if(NetworkUtils.isNetworkAvailable(context)){
            noteRepository.getGrades().observe(owner, new Observer<List<Nota>>() {
                @Override
                public void onChanged(@Nullable List<Nota> notes) {
                    note.addAll(notes);
                }
            });
            mViewModel.updateNotes(note);
        }
    }

}