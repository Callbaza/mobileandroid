package com.example.alex.myapplication.repo;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.alex.myapplication.vo.Nota;

@Database(entities = {Nota.class}, version = 1, exportSchema = false)
public abstract class NoteDatabase extends RoomDatabase {
    public abstract DaoAccess daoAccess();
}
