package com.example.alex.myapplication.vo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
public class Nota implements Serializable {
    @NonNull
    @PrimaryKey
    private Long id;
    private Byte nota;
    private String materie;
    private String data;

    public Nota(Long id, Byte nota, String materie, String data){
        this.id = id;
        this.nota = nota;
        this.materie = materie;
        this.data = data;
    }

    public Long getId() {
        return id;
    }

    public String getMaterie() {
        return materie;
    }

    public void setId(@NonNull Long id) {
        this.id = id;
    }

    public void setNota(Byte nota) {
        this.nota = nota;
    }

    public void setMaterie(String materie) {
        this.materie = materie;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Byte getNota() {
        return nota;
    }

    public String getData() {
        return data;
    }

    @Override
    public String toString() {
        return "Nota{" +
                "id=" + id +
                ", nota=" + nota +
                ", materie='" + materie + '\'' +
                ", data=" + data +
                '}';
    }
}
