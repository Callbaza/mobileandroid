package com.example.alex.myapplication.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;
import android.util.MutableBoolean;

import com.example.alex.myapplication.api.UsersResource;
import com.example.alex.myapplication.vo.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class LoginViewModel extends ViewModel {
    private static final String TAG = LoginViewModel.class.getCanonicalName();
    private MutableLiveData<Boolean> authenticated;

    public LiveData<Boolean> isAuthenticated(User user) {
        if (authenticated == null) {
            System.out.println("here");
            authenticated = new MutableLiveData<Boolean>();
            authenticate(user);
        }
        return authenticated;
    }


    private void authenticate(User user) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UsersResource.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        System.out.println("========================");
        UsersResource api = retrofit.create(UsersResource.class);
        System.out.println(user.toString());
        Call<ResponseBody> call = api.verifyUser(user);
        Log.d(TAG, "sent post");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "authentication succeeded");
                    authenticated.setValue(true);
                }
                else {
                    Log.e(TAG, "authentication failed");
                    authenticated.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "post failed to be sent", t);
                authenticated.setValue(false);

            }
        });
    }

}