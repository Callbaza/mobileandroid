package com.example.alex.myapplication.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alex.myapplication.R;
import com.example.alex.myapplication.repo.NoteRepository;
import com.example.alex.myapplication.sync.CheckConnection;
import com.example.alex.myapplication.viewmodel.NotesViewModel;
import com.example.alex.myapplication.vo.Nota;


import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class NoteListFragment extends Fragment {
    private NoteRepository noteRepository;
    private NotesViewModel mNotesViewModel;
    private RecyclerView mNoteList;
    final Handler handler = new Handler();
    private Runnable runnable;
    public static NoteListFragment newInstance() {
        return new NoteListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        noteRepository = new NoteRepository(getContext());
        noteRepository.deleteAllGrades();
        return inflater.inflate(R.layout.note_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNoteList = view.findViewById(R.id.note_list);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mNoteList.getContext(),1);
        mNoteList.addItemDecoration(dividerItemDecoration);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mNoteList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mNotesViewModel = ViewModelProviders.of(this).get(NotesViewModel.class);
        mNotesViewModel.getNotes().observe(this, new Observer<List<Nota>>() {
            @Override
            public void onChanged(@Nullable final List<Nota> notes) {
//                NoteListAdapter notesAdapter = new NoteListAdapter(getActivity(), notes);
//                mNoteList.setAdapter(notesAdapter);
                noteRepository.insertAllGrades(notes);

            }
        });
//        runnable = new Runnable() {
//            @Override
//            public void run() {
//                noteRepository.getGrades().observe(getViewLifecycleOwner(), new Observer<List<Nota>>() {
//                    @Override
//                    public void onChanged(@Nullable List<Nota> notes) {
//                        note.addAll(notes);
//                    }
//                });
//                mNotesViewModel.updateNotes(note);
//                System.out.println("here22222222222222222222222222222222222"+runnable);
//                handler.postDelayed(runnable, 5000);
//            }
//        };
//        System.out.println("here11111111111111111111111111111111111"+runnable);
//        handler.postDelayed(runnable, 5000);

        Timer timer = new Timer();
        System.out.println("TIMER CREATEEEEEEEEEEEEEEEEEED");
        timer.schedule(new CheckConnection(getContext(),getViewLifecycleOwner(), mNotesViewModel), 0, 25000);
    }

    @Override
    public void onResume() {
        super.onResume();


//        Timer timer = new Timer();
//        System.out.println("TIMER CREATEEEEEEEEEEEEEEEEEED");
//        timer.scheduleAtFixedRate(new CheckConnection(getContext(),getViewLifecycleOwner()), 0, 5000);
//        mNotesViewModel.getNotes().observe(this, new Observer<List<Nota>>() {
////            @Override
////            public void onChanged(@Nullable List<Nota> notes) {
////                NoteListAdapter notesAdapter = new NoteListAdapter(getActivity(), notes);
////                mNoteList.setAdapter(notesAdapter);
////            }
////        });
        noteRepository.getGrades().observe(this, new Observer<List<Nota>>() {
            @Override
            public void onChanged(@Nullable List<Nota> notes) {
                NoteListAdapter notesAdapter = new NoteListAdapter(getActivity(), notes);
                mNoteList.setAdapter(notesAdapter);
            }
        });
    }
}
