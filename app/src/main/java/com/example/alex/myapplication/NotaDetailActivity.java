package com.example.alex.myapplication;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.alex.myapplication.ui.NotaDetailFragment;

public class NotaDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nota_detail);
        Bundle bundle = getIntent().getExtras();
        Long id;
//        Byte nota;
//        String materie, data;
        if(bundle != null) {
            id = bundle.getLong("id");
//            nota = bundle.getByte("nota");
//            materie = bundle.getString("materie");
//            data = bundle.getString("data");
            this.setTitle("Nota cu id-ul " + id);
            NotaDetailFragment notaDetailFragment = new NotaDetailFragment();
            notaDetailFragment.setArguments(bundle);
            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.detailContainer, notaDetailFragment)
                        .commitNow();
            }
        }
        else{
            CharSequence text = "Nu s-a putut incarca nota.";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(this, text, duration);
            toast.show();        }
    }
}
