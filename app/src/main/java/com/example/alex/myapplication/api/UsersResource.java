package com.example.alex.myapplication.api;

import com.example.alex.myapplication.vo.Page;
import com.example.alex.myapplication.vo.User;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UsersResource {
    String BASE_URL = "http://192.168.137.1:8080/";
//    String BASE_URL = "http://192.168.1.6:8080/";


    @POST("/login")
    Call<ResponseBody> verifyUser(@Body User user);

    /*@GET("readAll")
    Call<List> getNote();*/

    /*@GET("readAll")
    List<Nota> getNote();*/
}
