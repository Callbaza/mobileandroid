package com.example.alex.myapplication.vo;

import java.util.List;

public class Page {
    private int number;
    private List<Nota> notes;

    public int getNumber() {
        return number;
    }

    public List<Nota> getNotes() {
        return notes;
    }

    public void toList(List<Nota> l){
        this.notes = l;
    }

    @Override
    public String toString() {
        return "Page{" +
                "number=" + number +
                ", notes=" + notes +
                '}';
    }
}
