package com.example.alex.myapplication.api;

import com.example.alex.myapplication.vo.Nota;
import com.example.alex.myapplication.vo.Page;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface NoteResource {
    String BASE_URL = "http://192.168.137.1:8080/grades/";
//    String BASE_URL = "http://192.168.1.6:8080/grades/";

    //10.0.2.2
    //192.168.56.1

    @GET("note")
    Call<Page> getNotes();

    @PUT("note/{id}")
    Call<ResponseBody> updateGrade(@Path("id") Long id, @Body Nota nota);

    @PUT("note")
    Call<ResponseBody> updateGrades(@Body List<Nota> note);
    /*@GET("readAll")
    List<Nota> getNote();*/

}
