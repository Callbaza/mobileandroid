package com.example.alex.myapplication.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.example.alex.myapplication.api.NoteResource;
import com.example.alex.myapplication.vo.Nota;
import com.example.alex.myapplication.vo.Page;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NotesViewModel extends ViewModel {
    private static final String TAG = NotesViewModel.class.getCanonicalName();
    private MutableLiveData<List<Nota>> notes = new MutableLiveData<List<Nota>>();
    private MutableLiveData<Boolean> updated;

    public LiveData<List<Nota>> getNotes() {
        loadNotes();
        return notes;
    }

    private void loadNotes() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NoteResource.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        System.out.println("========================");
        NoteResource api = retrofit.create(NoteResource.class);
        /*List<Nota> note = new ArrayList<>();
        String notele = api.getNotes().toString();
        System.out.println(notele);*/
        Call<Page> call = api.getNotes();
        //List<Nota> notele = api.getNote();
        //Call<List> call = api.getNote();
        //System.out.println(call);
        Log.d(TAG, "loadNotes");
        call.enqueue(new Callback<Page>() {
            @Override
            public void onResponse(Call<Page> call, Response<Page> response) {
                Log.d(TAG, "loadNotes succeeded");
                notes.setValue(response.body().getNotes());
            }

            @Override
            public void onFailure(Call<Page> call, Throwable t) {
                Log.e(TAG, "loadNotes failed", t);
            }
        });
    }

    public LiveData<Boolean> updateNotes(List<Nota> note) {
            updated = new MutableLiveData<Boolean>();
            updateGrades(note);

        return updated;
    }

    private void updateGrades(List<Nota> note) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NoteResource.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        System.out.println("========================");
        System.out.println("aiciiiiiii"+note);

        NoteResource api = retrofit.create(NoteResource.class);
        Call<ResponseBody> call = api.updateGrades(note);
        Log.d(TAG, "update notes");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "update grades succeded");
                    updated.setValue(true);
                }
                else {
                    Log.d(TAG, "update grades failed");
                    updated.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "update grades failed", t);
                updated.setValue(false);
            }
        });
    }

}
