package com.example.alex.myapplication.ui;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alex.myapplication.MainActivity;
import com.example.alex.myapplication.NotaDetailActivity;
import com.example.alex.myapplication.R;
import com.example.alex.myapplication.vo.Nota;


import java.util.List;

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.NoteViewHolder> {
    private final View.OnClickListener mOnClickListener = new ItemClickListener();
    private Context mContext;
    private List<Nota> mNotes;
    private RecyclerView mRecyclerView;

    public NoteListAdapter(Context context, List<Nota> notes) {
        this.mContext = context;
        this.mNotes = notes;
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.note_layout, parent, false);
        view.setOnClickListener(mOnClickListener);
        return new NoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
        Nota nota = mNotes.get(position);
        holder.textView.setText("nota: "+ nota.getNota()+", materia: "+ nota.getMaterie()+", data: "+nota.getData());
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.mRecyclerView = recyclerView;
    }

    @Override
    public int getItemCount() {
        return mNotes.size();
    }

    class NoteViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        NoteViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text_view);
        }
    }

    class ItemClickListener implements View.OnClickListener {
        @Override
        public void onClick(final View view) {
            int itemPosition = mRecyclerView.getChildLayoutPosition(view);
            Nota item = mNotes.get(itemPosition);
//            Toast.makeText(mContext, item.toString(), Toast.LENGTH_LONG).show();
            Intent intent = new Intent(mContext, NotaDetailActivity.class);
            intent.putExtra("id", item.getId());
            intent.putExtra("nota", item.getNota());
            intent.putExtra("materie", item.getMaterie());
            intent.putExtra("data", item.getData());
            mContext.startActivity(intent);
        }
    }
}
