package com.example.alex.myapplication.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.example.alex.myapplication.api.NoteResource;
import com.example.alex.myapplication.repo.NoteRepository;
import com.example.alex.myapplication.vo.Nota;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NotaDetailViewModel extends ViewModel {
    private static final String TAG = NotaDetailViewModel.class.getCanonicalName();
    private MutableLiveData<Boolean> updated;

    public LiveData<Boolean> isUpdated(Nota nota) {
        if (updated == null) {
            System.out.println("here");
            updated = new MutableLiveData<Boolean>();
            updateGrade(nota);
        }
        return updated;
    }

    private void updateGrade(Nota nota) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NoteResource.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final Long id = nota.getId();
        System.out.println("========================");
        NoteResource api = retrofit.create(NoteResource.class);
        Call<ResponseBody> call = api.updateGrade(nota.getId(), nota);
        //List<Nota> notele = api.getNote();
        //Call<List> call = api.getNote();
        //System.out.println(call);
        Log.d(TAG, "updateGrade");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "update grade with id " + id + " succeded");
                    updated.setValue(true);
                }
                else {
                    Log.d(TAG, "update grade with id " + id + " failed");
                    updated.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "update grade with id " + id + " failed", t);
                updated.setValue(false);
            }
        });
    }
}
