package com.example.alex.myapplication.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alex.myapplication.R;
import com.example.alex.myapplication.repo.NoteRepository;
import com.example.alex.myapplication.sync.CheckConnection;
import com.example.alex.myapplication.viewmodel.NotaDetailViewModel;
import com.example.alex.myapplication.vo.Nota;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Timer;

public class NotaDetailFragment extends Fragment {
    private NoteRepository noteRepository;
    private NotaDetailViewModel mViewModel;
    private EditText mNotaEditText;
    private EditText mMaterieEditText;
    private EditText mDataEditText;
    private Button mUpdateButton;

    public static NotaDetailFragment newInstance() {
        return new NotaDetailFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.nota_detail_fragment, container, false);
        // Set up the login form.
        mNotaEditText = view.findViewById(R.id.notaEditText);
        mMaterieEditText = view.findViewById(R.id.materiaEditText);
        mDataEditText = view.findViewById(R.id.dataEditText);
        mUpdateButton = view.findViewById(R.id.updateButton);
        mViewModel = ViewModelProviders.of(this).get(NotaDetailViewModel.class);
        noteRepository = new NoteRepository(getContext());
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(NotaDetailViewModel.class);
        final Bundle bundle=getArguments();
        final Long id = bundle.getLong("id");
        mNotaEditText.setText(""+bundle.getByte("nota"));
        mDataEditText.setText(bundle.getString("data"));
        mMaterieEditText.setText(bundle.getString("materie"));
        mUpdateButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Byte nota = Byte.parseByte(mNotaEditText.getText().toString());
                final String materie =  mMaterieEditText.getText().toString();
                final String data = mDataEditText.getText().toString();
                if (areValid(nota, materie, data, bundle)) {
                    noteRepository.updateGrade(new Nota(id, nota, materie, data));
                    mViewModel.isUpdated(new Nota(id, nota, materie, data)).observe(getActivity(), new Observer<Boolean>() {
                        @Override
                        public void onChanged(@Nullable Boolean updated) {
                            if (updated) {
                                Context context = getContext();
                                CharSequence text = "Nota modificata cu succes.";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                            } else {
                                Context context = getContext();
                                CharSequence text = "Nota nu a putut fi modificata.";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                            }
                        }
                    });
                }
            }
        });

    }

    private boolean areValid(Byte nota, String materie, String data, Bundle bundle){
        Context context = getContext();
        CharSequence text = "Modificati valorile!";
        int duration = Toast.LENGTH_SHORT;
        if (nota.equals(bundle.getByte("nota")) && materie.equals(bundle.getString("materie"))
                && data.equals(bundle.getString("data"))){
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            return false;
        }
        String errors = "";
        if (nota < 0 || nota > 10){
            errors += "Nota trebuie sa fie un numar intre 0 si 10!";
        }
        if (!isThisDateValid(data)){
            errors += "\nData trebuie sa fie de forma YYYY-MM-DD!";
        }
        if (errors=="") {
            return true;
        }
        context = getContext();
        text = errors;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
        return false;
    }

    private boolean isThisDateValid(String dateToValidate){
        if(dateToValidate == null){
            return false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);
        try {
            Date date = sdf.parse(dateToValidate);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
