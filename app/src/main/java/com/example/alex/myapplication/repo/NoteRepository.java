package com.example.alex.myapplication.repo;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;

import com.example.alex.myapplication.MainActivity;
import com.example.alex.myapplication.vo.Nota;

import java.security.acl.Owner;
import java.util.List;

public class NoteRepository {
    private String DB_NAME = "db_note";

    private NoteDatabase noteDatabase;
    public NoteRepository(Context context) {
        noteDatabase = Room.databaseBuilder(context, NoteDatabase.class, DB_NAME).build();
    }

    public LiveData<List<Nota>> getGrades() {
        return noteDatabase.daoAccess().fetchAllGrades();
    }

    public void updateGrade(final Nota nota) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().updateGrade(nota);
                return null;
            }
        }.execute();
    }

    public void insertAllGrades(List<Nota> note){
        for (Nota nota: note) {
            this.insertGrade(nota);
        }
    }

    public void deleteAllGrades(){
//        getGrades().observe(owner, new Observer<List<Nota>>() {
//            @Override
//            public void onChanged(@Nullable List<Nota> notes) {
//                for (Nota nota: notes){
//                    deleteGrade(nota);
//                }
//            }
//        });
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().nukeTable();
                return null;
            }
        }.execute();
    }

    public void insertGrade(final Nota nota) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().insertGrade(nota);
                return null;
            }
        }.execute();
    }

    public void deleteGrade(final Nota nota) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().deleteGrade(nota);
                return null;
            }
        }.execute();
    }
}
