package com.example.alex.myapplication.repo;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.alex.myapplication.vo.Nota;

import java.util.List;

@Dao
public interface DaoAccess {
    @Query("SELECT * FROM Nota")
    LiveData<List<Nota>> fetchAllGrades();

    @Update
    void updateGrade(Nota nota);

    @Insert
    Long insertGrade(Nota nota);

    @Delete
    void deleteGrade(Nota nota);

    @Query("DELETE FROM Nota")
    void nukeTable();
}
